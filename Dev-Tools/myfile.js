function onClickEvent(){
    console.group("onClickEvent");
    let myCount = 1;
    alert("You completed "+ myCount + " exercise(s)");
    myCount = nestedCall(myCount);
    console.error("You completed "+ myCount + " exercise(s)");
    console.groupEnd("onClickEvent");
}

function nestedCall(count){
    console.group("nestedcall");
    document.querySelector("body > ul > li:nth-child(1)").remove();
    console.log("reached nestedCall");
    console.groupEnd("nestedcall");

    return count+6;
}