package main

import (
	"fmt"
	"strings"
	"time"
)

func salamPembuka() {
	fmt.Printf("Selamat Datang di Portal Pemesanan Tiket %v\n", conferenceName)
	fmt.Printf(" Ticket Terbatas Hanya %v Tiket.\n", conferenceTickets)
	fmt.Println("Grab It Fast!!!")
}

func inputData() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint

	fmt.Println("Nama Depan: ")
	fmt.Scan(&firstName)

	fmt.Println("Nama Belakang: ")
	fmt.Scan(&lastName)

	fmt.Println("Alamat Email: ")
	fmt.Scan(&email)

	fmt.Println("Jumlah Tiket yang Dipesan: ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func validateUserInput(email string, userTickets uint) (bool, bool) {
	isValidEmail := strings.Contains(email, "@")
	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets
	return isValidEmail, isValidTicketNumber
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

func pesanTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Printf("Terimakasih %v %v Atas Pemesanan %v tickets. Tiket Akan Dikirimkan ke %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("Sisa tiket untuk acara %v adalah %v\n", conferenceName, remainingTickets)
}

func kirimTicket(userTickets uint, firstName string, lastName string, email string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	fmt.Println("#################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("#################")
	wg.Done()
}
