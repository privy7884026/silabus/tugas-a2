package main

import (
	"fmt"
	"sync"
)

const conferenceTickets int = 50

var conferenceName = "PESTAPORA"
var remainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {

	salamPembuka()

	for {

		firstName, lastName, email, userTickets := inputData()
		isValidEmail, isValidTicketNumber := validateUserInput(email, userTickets)

		if isValidEmail && isValidTicketNumber {

			pesanTicket(userTickets, firstName, lastName, email)

			wg.Add(1)
			go kirimTicket(userTickets, firstName, lastName, email)

			firstNames := getFirstNames()
			fmt.Printf("Daftar Peserta: %v\n", firstNames)

			if remainingTickets == 0 {
				fmt.Println("================Tiket Habis================")
				fmt.Printf("Daftar Pemesan\n %v\n", bookings)
				break
			}
		} else {
			if !isValidEmail {
				fmt.Println("Format email yang dimasukan salah")
			}
			if !isValidTicketNumber {
				fmt.Println("Jumlah pemesanan ticket melebisi sisa tiket yang ada")
			}
		}
		wg.Wait()
	}
}
